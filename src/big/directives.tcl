#
# Copyright 2016 Andrea Solazzo, Emanuele Del Sozzo, Gianluca Durelli, Matteo De Silvestri, Irene De Rose
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# 	http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set_directive_interface -mode ap_ctrl_none -register -latency 0 "cnn"
set_directive_interface -mode axis "cnn" streamOut
set_directive_interface -mode axis "cnn" streamIn
