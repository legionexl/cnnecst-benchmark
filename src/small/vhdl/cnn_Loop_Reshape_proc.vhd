-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.4
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cnn_Loop_Reshape_proc is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_continue : IN STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    p1_address0 : OUT STD_LOGIC_VECTOR (7 downto 0);
    p1_ce0 : OUT STD_LOGIC;
    p1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    lin_address0 : OUT STD_LOGIC_VECTOR (7 downto 0);
    lin_ce0 : OUT STD_LOGIC;
    lin_we0 : OUT STD_LOGIC;
    lin_d0 : OUT STD_LOGIC_VECTOR (31 downto 0) );
end;


architecture behav of cnn_Loop_Reshape_proc is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_ST_st1_fsm_0 : STD_LOGIC_VECTOR (4 downto 0) := "00001";
    constant ap_ST_st2_fsm_1 : STD_LOGIC_VECTOR (4 downto 0) := "00010";
    constant ap_ST_st3_fsm_2 : STD_LOGIC_VECTOR (4 downto 0) := "00100";
    constant ap_ST_st4_fsm_3 : STD_LOGIC_VECTOR (4 downto 0) := "01000";
    constant ap_ST_st5_fsm_4 : STD_LOGIC_VECTOR (4 downto 0) := "10000";
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_lv1_1 : STD_LOGIC_VECTOR (0 downto 0) := "1";
    constant ap_const_lv32_1 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000001";
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv32_2 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000010";
    constant ap_const_lv32_3 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000011";
    constant ap_const_lv3_0 : STD_LOGIC_VECTOR (2 downto 0) := "000";
    constant ap_const_lv32_4 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000100";
    constant ap_const_lv3_6 : STD_LOGIC_VECTOR (2 downto 0) := "110";
    constant ap_const_lv3_1 : STD_LOGIC_VECTOR (2 downto 0) := "001";
    constant ap_const_lv2_0 : STD_LOGIC_VECTOR (1 downto 0) := "00";

    signal ap_done_reg : STD_LOGIC := '0';
    signal ap_CS_fsm : STD_LOGIC_VECTOR (4 downto 0) := "00001";
    attribute fsm_encoding : string;
    attribute fsm_encoding of ap_CS_fsm : signal is "none";
    signal ap_sig_cseq_ST_st1_fsm_0 : STD_LOGIC;
    signal ap_sig_bdd_24 : BOOLEAN;
    signal k_fu_102_p2 : STD_LOGIC_VECTOR (2 downto 0);
    signal k_reg_292 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_sig_cseq_ST_st2_fsm_1 : STD_LOGIC;
    signal ap_sig_bdd_51 : BOOLEAN;
    signal m_cast_i_fu_118_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal m_cast_i_reg_297 : STD_LOGIC_VECTOR (8 downto 0);
    signal exitcond8_i_fu_96_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_3_cast_fu_152_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_3_cast_reg_302 : STD_LOGIC_VECTOR (7 downto 0);
    signal i_fu_162_p2 : STD_LOGIC_VECTOR (2 downto 0);
    signal i_reg_310 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_sig_cseq_ST_st3_fsm_2 : STD_LOGIC;
    signal ap_sig_bdd_68 : BOOLEAN;
    signal n_cast_i_cast_fu_198_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal n_cast_i_cast_reg_315 : STD_LOGIC_VECTOR (7 downto 0);
    signal exitcond7_i_fu_156_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_7_fu_231_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal tmp_7_reg_320 : STD_LOGIC_VECTOR (8 downto 0);
    signal j_fu_247_p2 : STD_LOGIC_VECTOR (2 downto 0);
    signal j_reg_328 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_sig_cseq_ST_st4_fsm_3 : STD_LOGIC;
    signal ap_sig_bdd_84 : BOOLEAN;
    signal n_1_fu_262_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal n_1_reg_333 : STD_LOGIC_VECTOR (8 downto 0);
    signal exitcond6_i_fu_241_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal k_3_i_reg_63 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_sig_bdd_101 : BOOLEAN;
    signal i_5_i_reg_74 : STD_LOGIC_VECTOR (2 downto 0);
    signal j_4_i_reg_85 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_sig_cseq_ST_st5_fsm_4 : STD_LOGIC;
    signal ap_sig_bdd_115 : BOOLEAN;
    signal tmp_8_cast_fu_276_p1 : STD_LOGIC_VECTOR (63 downto 0);
    signal tmp_27_i_fu_284_p1 : STD_LOGIC_VECTOR (63 downto 0);
    signal m_fu_108_p4 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_1_fu_122_p3 : STD_LOGIC_VECTOR (5 downto 0);
    signal tmp_2_fu_134_p3 : STD_LOGIC_VECTOR (3 downto 0);
    signal p_shl_cast_fu_130_p1 : STD_LOGIC_VECTOR (6 downto 0);
    signal p_shl1_cast_fu_142_p1 : STD_LOGIC_VECTOR (6 downto 0);
    signal tmp_3_fu_146_p2 : STD_LOGIC_VECTOR (6 downto 0);
    signal p_shl4_i_fu_168_p3 : STD_LOGIC_VECTOR (5 downto 0);
    signal p_shl5_i_fu_180_p3 : STD_LOGIC_VECTOR (3 downto 0);
    signal p_shl4_cast_i_fu_176_p1 : STD_LOGIC_VECTOR (6 downto 0);
    signal p_shl5_cast_i_fu_188_p1 : STD_LOGIC_VECTOR (6 downto 0);
    signal n_fu_192_p2 : STD_LOGIC_VECTOR (6 downto 0);
    signal tmp_15_i_cast_fu_202_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_4_fu_206_p2 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_5_fu_211_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal p_shl2_cast_fu_215_p3 : STD_LOGIC_VECTOR (8 downto 0);
    signal p_shl3_cast_fu_223_p3 : STD_LOGIC_VECTOR (8 downto 0);
    signal j_4_cast14_i_cast_fu_237_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_fu_253_p2 : STD_LOGIC_VECTOR (7 downto 0);
    signal tmp_cast_fu_258_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal tmp_26_i_cast_fu_267_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal tmp_8_fu_271_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal n_2_cast_i_fu_281_p1 : STD_LOGIC_VECTOR (31 downto 0);
    signal ap_NS_fsm : STD_LOGIC_VECTOR (4 downto 0);


begin




    -- the current state (ap_CS_fsm) of the state machine. --
    ap_CS_fsm_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_CS_fsm <= ap_ST_st1_fsm_0;
            else
                ap_CS_fsm <= ap_NS_fsm;
            end if;
        end if;
    end process;


    -- ap_done_reg assign process. --
    ap_done_reg_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_done_reg <= ap_const_logic_0;
            else
                if ((ap_const_logic_1 = ap_continue)) then 
                    ap_done_reg <= ap_const_logic_0;
                elsif (((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1) and not((exitcond8_i_fu_96_p2 = ap_const_lv1_0)))) then 
                    ap_done_reg <= ap_const_logic_1;
                end if; 
            end if;
        end if;
    end process;


    -- i_5_i_reg_74 assign process. --
    i_5_i_reg_74_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st4_fsm_3) and not((ap_const_lv1_0 = exitcond6_i_fu_241_p2)))) then 
                i_5_i_reg_74 <= i_reg_310;
            elsif (((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1) and (exitcond8_i_fu_96_p2 = ap_const_lv1_0))) then 
                i_5_i_reg_74 <= ap_const_lv3_0;
            end if; 
        end if;
    end process;

    -- j_4_i_reg_85 assign process. --
    j_4_i_reg_85_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st3_fsm_2) and (ap_const_lv1_0 = exitcond7_i_fu_156_p2))) then 
                j_4_i_reg_85 <= ap_const_lv3_0;
            elsif ((ap_const_logic_1 = ap_sig_cseq_ST_st5_fsm_4)) then 
                j_4_i_reg_85 <= j_reg_328;
            end if; 
        end if;
    end process;

    -- k_3_i_reg_63 assign process. --
    k_3_i_reg_63_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st1_fsm_0) and not(ap_sig_bdd_101))) then 
                k_3_i_reg_63 <= ap_const_lv3_0;
            elsif (((ap_const_logic_1 = ap_sig_cseq_ST_st3_fsm_2) and not((ap_const_lv1_0 = exitcond7_i_fu_156_p2)))) then 
                k_3_i_reg_63 <= k_reg_292;
            end if; 
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_sig_cseq_ST_st3_fsm_2)) then
                i_reg_310 <= i_fu_162_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_sig_cseq_ST_st4_fsm_3)) then
                j_reg_328 <= j_fu_247_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1)) then
                k_reg_292 <= k_fu_102_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1) and (exitcond8_i_fu_96_p2 = ap_const_lv1_0))) then
                    m_cast_i_reg_297(7 downto 2) <= m_cast_i_fu_118_p1(7 downto 2);
                    tmp_3_cast_reg_302(7 downto 1) <= tmp_3_cast_fu_152_p1(7 downto 1);
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st4_fsm_3) and (ap_const_lv1_0 = exitcond6_i_fu_241_p2))) then
                n_1_reg_333 <= n_1_fu_262_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_sig_cseq_ST_st3_fsm_2) and (ap_const_lv1_0 = exitcond7_i_fu_156_p2))) then
                    n_cast_i_cast_reg_315(7 downto 1) <= n_cast_i_cast_fu_198_p1(7 downto 1);
                    tmp_7_reg_320(8 downto 1) <= tmp_7_fu_231_p2(8 downto 1);
            end if;
        end if;
    end process;
    m_cast_i_reg_297(1 downto 0) <= "00";
    m_cast_i_reg_297(8) <= '0';
    tmp_3_cast_reg_302(0) <= '0';
    n_cast_i_cast_reg_315(0) <= '0';
    tmp_7_reg_320(0) <= '0';

    -- the next state (ap_NS_fsm) of the state machine. --
    ap_NS_fsm_assign_proc : process (ap_CS_fsm, exitcond8_i_fu_96_p2, exitcond7_i_fu_156_p2, exitcond6_i_fu_241_p2, ap_sig_bdd_101)
    begin
        case ap_CS_fsm is
            when ap_ST_st1_fsm_0 => 
                if (not(ap_sig_bdd_101)) then
                    ap_NS_fsm <= ap_ST_st2_fsm_1;
                else
                    ap_NS_fsm <= ap_ST_st1_fsm_0;
                end if;
            when ap_ST_st2_fsm_1 => 
                if (not((exitcond8_i_fu_96_p2 = ap_const_lv1_0))) then
                    ap_NS_fsm <= ap_ST_st1_fsm_0;
                else
                    ap_NS_fsm <= ap_ST_st3_fsm_2;
                end if;
            when ap_ST_st3_fsm_2 => 
                if (not((ap_const_lv1_0 = exitcond7_i_fu_156_p2))) then
                    ap_NS_fsm <= ap_ST_st2_fsm_1;
                else
                    ap_NS_fsm <= ap_ST_st4_fsm_3;
                end if;
            when ap_ST_st4_fsm_3 => 
                if ((ap_const_lv1_0 = exitcond6_i_fu_241_p2)) then
                    ap_NS_fsm <= ap_ST_st5_fsm_4;
                else
                    ap_NS_fsm <= ap_ST_st3_fsm_2;
                end if;
            when ap_ST_st5_fsm_4 => 
                ap_NS_fsm <= ap_ST_st4_fsm_3;
            when others =>  
                ap_NS_fsm <= "XXXXX";
        end case;
    end process;

    -- ap_done assign process. --
    ap_done_assign_proc : process(ap_done_reg, ap_sig_cseq_ST_st2_fsm_1, exitcond8_i_fu_96_p2)
    begin
        if (((ap_const_logic_1 = ap_done_reg) or ((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1) and not((exitcond8_i_fu_96_p2 = ap_const_lv1_0))))) then 
            ap_done <= ap_const_logic_1;
        else 
            ap_done <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_idle assign process. --
    ap_idle_assign_proc : process(ap_start, ap_sig_cseq_ST_st1_fsm_0)
    begin
        if ((not((ap_const_logic_1 = ap_start)) and (ap_const_logic_1 = ap_sig_cseq_ST_st1_fsm_0))) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_ready assign process. --
    ap_ready_assign_proc : process(ap_sig_cseq_ST_st2_fsm_1, exitcond8_i_fu_96_p2)
    begin
        if (((ap_const_logic_1 = ap_sig_cseq_ST_st2_fsm_1) and not((exitcond8_i_fu_96_p2 = ap_const_lv1_0)))) then 
            ap_ready <= ap_const_logic_1;
        else 
            ap_ready <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_sig_bdd_101 assign process. --
    ap_sig_bdd_101_assign_proc : process(ap_start, ap_done_reg)
    begin
                ap_sig_bdd_101 <= ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1));
    end process;


    -- ap_sig_bdd_115 assign process. --
    ap_sig_bdd_115_assign_proc : process(ap_CS_fsm)
    begin
                ap_sig_bdd_115 <= (ap_const_lv1_1 = ap_CS_fsm(4 downto 4));
    end process;


    -- ap_sig_bdd_24 assign process. --
    ap_sig_bdd_24_assign_proc : process(ap_CS_fsm)
    begin
                ap_sig_bdd_24 <= (ap_CS_fsm(0 downto 0) = ap_const_lv1_1);
    end process;


    -- ap_sig_bdd_51 assign process. --
    ap_sig_bdd_51_assign_proc : process(ap_CS_fsm)
    begin
                ap_sig_bdd_51 <= (ap_const_lv1_1 = ap_CS_fsm(1 downto 1));
    end process;


    -- ap_sig_bdd_68 assign process. --
    ap_sig_bdd_68_assign_proc : process(ap_CS_fsm)
    begin
                ap_sig_bdd_68 <= (ap_const_lv1_1 = ap_CS_fsm(2 downto 2));
    end process;


    -- ap_sig_bdd_84 assign process. --
    ap_sig_bdd_84_assign_proc : process(ap_CS_fsm)
    begin
                ap_sig_bdd_84 <= (ap_const_lv1_1 = ap_CS_fsm(3 downto 3));
    end process;


    -- ap_sig_cseq_ST_st1_fsm_0 assign process. --
    ap_sig_cseq_ST_st1_fsm_0_assign_proc : process(ap_sig_bdd_24)
    begin
        if (ap_sig_bdd_24) then 
            ap_sig_cseq_ST_st1_fsm_0 <= ap_const_logic_1;
        else 
            ap_sig_cseq_ST_st1_fsm_0 <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_sig_cseq_ST_st2_fsm_1 assign process. --
    ap_sig_cseq_ST_st2_fsm_1_assign_proc : process(ap_sig_bdd_51)
    begin
        if (ap_sig_bdd_51) then 
            ap_sig_cseq_ST_st2_fsm_1 <= ap_const_logic_1;
        else 
            ap_sig_cseq_ST_st2_fsm_1 <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_sig_cseq_ST_st3_fsm_2 assign process. --
    ap_sig_cseq_ST_st3_fsm_2_assign_proc : process(ap_sig_bdd_68)
    begin
        if (ap_sig_bdd_68) then 
            ap_sig_cseq_ST_st3_fsm_2 <= ap_const_logic_1;
        else 
            ap_sig_cseq_ST_st3_fsm_2 <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_sig_cseq_ST_st4_fsm_3 assign process. --
    ap_sig_cseq_ST_st4_fsm_3_assign_proc : process(ap_sig_bdd_84)
    begin
        if (ap_sig_bdd_84) then 
            ap_sig_cseq_ST_st4_fsm_3 <= ap_const_logic_1;
        else 
            ap_sig_cseq_ST_st4_fsm_3 <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_sig_cseq_ST_st5_fsm_4 assign process. --
    ap_sig_cseq_ST_st5_fsm_4_assign_proc : process(ap_sig_bdd_115)
    begin
        if (ap_sig_bdd_115) then 
            ap_sig_cseq_ST_st5_fsm_4 <= ap_const_logic_1;
        else 
            ap_sig_cseq_ST_st5_fsm_4 <= ap_const_logic_0;
        end if; 
    end process;

    exitcond6_i_fu_241_p2 <= "1" when (j_4_i_reg_85 = ap_const_lv3_6) else "0";
    exitcond7_i_fu_156_p2 <= "1" when (i_5_i_reg_74 = ap_const_lv3_6) else "0";
    exitcond8_i_fu_96_p2 <= "1" when (k_3_i_reg_63 = ap_const_lv3_6) else "0";
    i_fu_162_p2 <= std_logic_vector(unsigned(i_5_i_reg_74) + unsigned(ap_const_lv3_1));
    j_4_cast14_i_cast_fu_237_p1 <= std_logic_vector(resize(unsigned(j_4_i_reg_85),8));
    j_fu_247_p2 <= std_logic_vector(unsigned(j_4_i_reg_85) + unsigned(ap_const_lv3_1));
    k_fu_102_p2 <= std_logic_vector(unsigned(k_3_i_reg_63) + unsigned(ap_const_lv3_1));
    lin_address0 <= tmp_27_i_fu_284_p1(8 - 1 downto 0);

    -- lin_ce0 assign process. --
    lin_ce0_assign_proc : process(ap_sig_cseq_ST_st5_fsm_4)
    begin
        if ((ap_const_logic_1 = ap_sig_cseq_ST_st5_fsm_4)) then 
            lin_ce0 <= ap_const_logic_1;
        else 
            lin_ce0 <= ap_const_logic_0;
        end if; 
    end process;

    lin_d0 <= p1_q0;

    -- lin_we0 assign process. --
    lin_we0_assign_proc : process(ap_sig_cseq_ST_st5_fsm_4)
    begin
        if (((ap_const_logic_1 = ap_sig_cseq_ST_st5_fsm_4))) then 
            lin_we0 <= ap_const_logic_1;
        else 
            lin_we0 <= ap_const_logic_0;
        end if; 
    end process;

    m_cast_i_fu_118_p1 <= std_logic_vector(resize(unsigned(m_fu_108_p4),9));
    m_fu_108_p4 <= ((k_3_i_reg_63 & k_3_i_reg_63) & ap_const_lv2_0);
    n_1_fu_262_p2 <= std_logic_vector(unsigned(m_cast_i_reg_297) + unsigned(tmp_cast_fu_258_p1));
        n_2_cast_i_fu_281_p1 <= std_logic_vector(resize(signed(n_1_reg_333),32));

        n_cast_i_cast_fu_198_p1 <= std_logic_vector(resize(signed(n_fu_192_p2),8));

    n_fu_192_p2 <= std_logic_vector(unsigned(p_shl4_cast_i_fu_176_p1) - unsigned(p_shl5_cast_i_fu_188_p1));
    p1_address0 <= tmp_8_cast_fu_276_p1(8 - 1 downto 0);

    -- p1_ce0 assign process. --
    p1_ce0_assign_proc : process(ap_sig_cseq_ST_st4_fsm_3)
    begin
        if ((ap_const_logic_1 = ap_sig_cseq_ST_st4_fsm_3)) then 
            p1_ce0 <= ap_const_logic_1;
        else 
            p1_ce0 <= ap_const_logic_0;
        end if; 
    end process;

    p_shl1_cast_fu_142_p1 <= std_logic_vector(resize(unsigned(tmp_2_fu_134_p3),7));
    p_shl2_cast_fu_215_p3 <= (tmp_5_fu_211_p1 & ap_const_lv3_0);
    p_shl3_cast_fu_223_p3 <= (tmp_4_fu_206_p2 & ap_const_lv1_0);
    p_shl4_cast_i_fu_176_p1 <= std_logic_vector(resize(unsigned(p_shl4_i_fu_168_p3),7));
    p_shl4_i_fu_168_p3 <= (i_5_i_reg_74 & ap_const_lv3_0);
    p_shl5_cast_i_fu_188_p1 <= std_logic_vector(resize(unsigned(p_shl5_i_fu_180_p3),7));
    p_shl5_i_fu_180_p3 <= (i_5_i_reg_74 & ap_const_lv1_0);
    p_shl_cast_fu_130_p1 <= std_logic_vector(resize(unsigned(tmp_1_fu_122_p3),7));
    tmp_15_i_cast_fu_202_p1 <= std_logic_vector(resize(unsigned(i_5_i_reg_74),8));
    tmp_1_fu_122_p3 <= (k_3_i_reg_63 & ap_const_lv3_0);
    tmp_26_i_cast_fu_267_p1 <= std_logic_vector(resize(unsigned(j_4_i_reg_85),9));
    tmp_27_i_fu_284_p1 <= std_logic_vector(resize(unsigned(n_2_cast_i_fu_281_p1),64));
    tmp_2_fu_134_p3 <= (k_3_i_reg_63 & ap_const_lv1_0);
        tmp_3_cast_fu_152_p1 <= std_logic_vector(resize(signed(tmp_3_fu_146_p2),8));

    tmp_3_fu_146_p2 <= std_logic_vector(unsigned(p_shl_cast_fu_130_p1) - unsigned(p_shl1_cast_fu_142_p1));
    tmp_4_fu_206_p2 <= std_logic_vector(signed(tmp_3_cast_reg_302) + signed(tmp_15_i_cast_fu_202_p1));
    tmp_5_fu_211_p1 <= tmp_4_fu_206_p2(6 - 1 downto 0);
    tmp_7_fu_231_p2 <= std_logic_vector(unsigned(p_shl2_cast_fu_215_p3) - unsigned(p_shl3_cast_fu_223_p3));
    tmp_8_cast_fu_276_p1 <= std_logic_vector(resize(unsigned(tmp_8_fu_271_p2),64));
    tmp_8_fu_271_p2 <= std_logic_vector(unsigned(tmp_7_reg_320) + unsigned(tmp_26_i_cast_fu_267_p1));
        tmp_cast_fu_258_p1 <= std_logic_vector(resize(signed(tmp_fu_253_p2),9));

    tmp_fu_253_p2 <= std_logic_vector(unsigned(j_4_cast14_i_cast_fu_237_p1) + unsigned(n_cast_i_cast_reg_315));
end behav;
